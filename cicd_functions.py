import requests
from jira import JIRA
import re
from dotenv import load_dotenv
import os
import git
import subprocess
import logging
load_dotenv()

gitlab_api_url = "https://gitlab.com/api/v4/projects/xor-singhakanksha%2Fsdlc-test/"
gitlab_token ="glpat-SBGkmUgNkDpGUXzrgHpw"
headers = {
        "PRIVATE-TOKEN": gitlab_token,
    }


logging.basicConfig(level=logging.ERROR)

GITLAB_API_URL = os.getenv('GITLAB_API_URL')
GITLAB_ISSUE_URL = f"{GITLAB_API_URL}/issues"  # Replace with the correct GitLab API URL
GITLAB_ACCESS_TOKEN = os.getenv('GITLAB_ACCESS_TOKEN')
gitlab_headers = {"PRIVATE-TOKEN": GITLAB_ACCESS_TOKEN }
GIT_SSH_URL = os.getenv('GIT_SSH_URL')



JIRA_SERVER_URL = os.getenv('JIRA_SERVER_URL')
JIRA_USER = os.getenv('JIRA_USER')
JIRA_TOKEN = os.getenv('JIRA_TOKEN')
JIRA_PROJECT_KEY = os.getenv('JIRA_PROJECT_KEY')
JIRA_ISSUE_TYPE =os.getenv('JIRA_ISSUE_TYPE')
jira_options = {'server': JIRA_SERVER_URL}
JIRA_CLIENT = JIRA(options=jira_options, basic_auth=(JIRA_USER, JIRA_TOKEN))


def create_jira_ticket(summary, description, issuetype_name):
    issue_dict = {
        'project': {'key': JIRA_PROJECT_KEY},
        'summary': summary,
        'description': description,
        'issuetype': {'name': issuetype_name},
    }
    new_issue=None
    try:
     new_issue = JIRA_CLIENT.create_issue(fields=issue_dict)
    except Exception as e:
     logging.error(f"Jira client error: {e}")
    return new_issue

def get_jira_issue_title(issue_id):
    try:
        # Retrieve the issue
        issue = JIRA_CLIENT.issue(issue_id)
        
        # Extract and return the issue title
        return issue.fields.summary
    except Exception as e:
        print(f"Failed to fetch Jira issue. Error: {e}")
        return None


def create_gitlab_issue(title, body):
    data = {
        "title": title,
        "description": body,
    }
    response = requests.post(GITLAB_ISSUE_URL, headers=gitlab_headers, data=data)

    print(response,GITLAB_ISSUE_URL )
    if response.status_code == 201:
      return {"status_code": response.status_code, "gitlab_issue_id": str(response.json()["iid"])}
    else:
        print(f"Failed to create GitLab issue. Status code: {response.status_code}")
        return {"status_code": response.status_code}
    
    
def tag_github_issue_with_jira(gitlab_issue_id, jira_issue_id):
    data = {
        "description":  f'Related JIRA issue: [{jira_issue_id}]({JIRA_SERVER_URL}/browse/{jira_issue_id})'
       
    }

    update_url = f"{GITLAB_ISSUE_URL}/{gitlab_issue_id}"
    response = requests.put(update_url, headers=gitlab_headers, data=data)

    print('ccccccccc', response, data)
    if response.status_code == 200:
        print("GitLab issue updated successfully.")
        return True
    else:
        print(f"Failed to update GitLab issue. Status code: {response.status_code}")
        return False
    

    
def branch_exists_on_gitlab( branch_name):
    api_endpoint = f"{GITLAB_API_URL}/repository/branches/{branch_name}"

    response = requests.get(api_endpoint, headers=gitlab_headers)
    print('resssssssss', response)
    if response.status_code == 200:
        return True
    elif response.status_code == 404:
        return False
    else:
        print(f"Failed to check branch existence. Status code: {response.status_code}")
        return None
    
def branch_exists_locally(branch_name):
    try:
        subprocess.check_output(['git', 'rev-parse', '--verify', branch_name])
        return True
    except subprocess.CalledProcessError:
        return False
    

def create_branch_commit_and_push(issue_key,git_issue_id, branch_name, commit_summary):
    try:

        # Create a new branch
        branch_name_with_issue = f"{issue_key}-{branch_name}"
        subprocess.run(["git", "checkout", "-b", branch_name_with_issue])

        subprocess.run(["git", "add", "."])
        subprocess.run(["git", "commit", "-m", f"{issue_key} {commit_summary}"])

        subprocess.run(["git", "push", GIT_SSH_URL, branch_name_with_issue])

        
        mr_title = "Feature: "+str(commit_summary)
        response = create_merge_request(branch_name_with_issue,'main', mr_title, git_issue_id)
        return response
        print(f"Branch '{branch_name_with_issue}' created, commit linked to Jira issue, and branch pushed to GitLab successfully.")
    except Exception as e:
        print(f"Error: {e}")


def create_merge_request(source_branch, target_branch, title, git_issue_id):
    url = f"{GITLAB_API_URL}/merge_requests"
    print(url)
    data = {
        "source_branch": source_branch,
        "target_branch": target_branch,
        "title": title,
        'description': f'Closes #{git_issue_id}'
    }
   
    response = requests.post(url, headers=gitlab_headers, data=data)
    print(response)
    if response.status_code == 201:
        print(f"Merge Request '{title}' created successfully.")
        return response.json()
    else:
        print(f"Failed to create Merge Request. Status code: {response.status_code}")
        print(response.text)
        return response.text 


def label_github_issue_with_jira(github_repo, issue_number, jira_issue_key):
    # URL for the GitHub issue labels API
    url = f'https://api.github.com/repos/{github_repo}/issues/{issue_number}/labels'
    print('uuuuuuuuuuuuuu', url)
    # Prepare the label with the JIRA issue key
    labels = [jira_issue_key]
    print('i am hereeeeeeeeeeeeee55ee>>>')
    # Make the request to add the label to the GitHub issue
    response = requests.post(url, json={"labels": labels}, headers=headers)
    print('i am hereeeeeeeeeeeeeeee>>>', response)
    # Check if the request was successful
    if response.status_code in (200, 201):
        print(f"Successfully labeled GitHub issue #{issue_number} with JIRA issue {jira_issue_key}")
        return {"success":True}
    else:
        print(f"Failed to label GitHub issue #{issue_number} with JIRA issue {jira_issue_key}")
        print(f"Response: {response.content}")
        return {"success":False}
    

def get_latest_pipeline_report():
    # GitLab API endpoint for getting the latest pipeline report
    api_url = f'{GITLAB_API_URL}/pipelines?order_by=id&sort=desc&per_page=1'
    # Make the API request
    response = requests.get(api_url, headers=headers)
    print(response)
    # Check if the request was successful (status code 200)
    if response.status_code == 200:
        pipeline_data = response.json()
        print(pipeline_data)
        # Extract information from the latest pipeline
        if pipeline_data:
            latest_pipeline = pipeline_data[0]
            pipeline_id = latest_pipeline.get('id')
            pipeline_status = latest_pipeline.get('status')
            pipeline_web_url = latest_pipeline.get('web_url')

            print(f"Latest Pipeline ID: {pipeline_id}")
            print(f"Status: {pipeline_status}")
            print(f"Web URL: {pipeline_web_url}")
            # You can further extend this to retrieve additional information if needed
            job_details= get_job_urls_from_pipeline_url(pipeline_web_url,pipeline_id)
            print('kkkkkkkkk',job_details["job_name"])
            job_name=job_details["job_name"]
            file_type="trace"
            artifact_url = f'https://gitlab.com/xor-singhakanksha/sdlc-test/-/jobs/{job_details["job_id"]}/artifacts/download?file_type=trace'
            response = requests.get(artifact_url, headers=headers)
            print('respobseee',response, artifact_url)
            if response.status_code == 200:
               artifact_filename = f'job_{job_details["job_id"]}_artifact_{file_type}.txt'
               with open(artifact_filename, 'wb') as artifact_file:
                artifact_file.write(response.content)

                print(f"Downloaded job artifact: {artifact_filename}")
                return str(artifact_filename)

            else:
               print(f"Failed to download job artifact. Status code: {response.status_code}")
        else:
            print("No pipelines found for the project.")
    else:
        print(f"Failed to retrieve pipeline information. Status code: {response.status_code}")


def get_job_urls_from_pipeline_url(pipeline_url, pipeline_id):
    # Extract project ID and pipeline ID from the URL
        # GitLab API endpoint for getting the pipeline details
        api_url = f'{gitlab_api_url}/pipelines/{pipeline_id}/jobs'
        # Make the API request
        response = requests.get(api_url, headers=headers)
        print('--------------', response)
        # Check if the request was successful (status code 200)
        if response.status_code == 200:
            jobs_data = response.json()
            # Extract and print job URLs
            for job in jobs_data:
                    job_url = job.get('web_url')
                    job_name = job.get('name')
                    job_id = job.get('id')
                    if(job_name=="unit-test-job"):
                     return {"job_url":job_url,"job_id":job_id, "job_name":job_name}
            else:
                print("No jobs found in the pipeline.")

        else:
            print(f"Failed to retrieve pipeline information. Status code: {response.status_code}")


def download_job_artifact_log(job_url):
    job_id = "6080058715"
    # Make the API request to get job details
    response = requests.get(job_url, headers=headers)

    

    print("hereeeeeeeeeeeeee", response.status_code)
    
    # Check if the request was successful (status code 200)
    if response.status_code == 200:
        try:
            job_data = response.json()
            print(job_data, 'jobbbbbbb')

            # Extract and print relevant job information
            job_status = job_data.get('status')
            job_name = job_data.get('name')
            job_response = job_data.get('artifacts_file', {}).get('filename', f'job_{job_id}_output.txt')

            # Download the job artifacts (log file)
            download_url = f'https://gitlab.com/api/v4/projects/{job_data["project_id"]}/jobs/{job_id}/artifacts/{job_response}'
            response = requests.get(download_url, headers=headers)

            if response.status_code == 200:
                with open(job_response, 'wb') as output_file:
                    output_file.write(response.content)

                print(f"Downloaded job artifact log: {job_response}")
            else:
                print(f"Failed to download job artifact. Status code: {response.status_code}")

        except ValueError as e:
            print(f"Error decoding JSON: {e}")
            print("Response content:")
            print(response.content)

    else:
        print(f"Failed to retrieve job information. Status code: {response.status_code}")


def add_attachment_to_jira(issue_key, file_path):
    issue = JIRA_CLIENT.issue(issue_key)

    # Attach the file to the Jira issue
    with open(file_path, 'rb') as attachment:
        JIRA_CLIENT.add_attachment(issue=issue, attachment=attachment, filename=file_path)

    print(f"Attachment added successfully to issue {issue_key}")


def remove_characters(text, characters):
    for char in characters:
        text = text.replace(char, '')
    underscored_text = text.replace(' ', '_')
    return underscored_text