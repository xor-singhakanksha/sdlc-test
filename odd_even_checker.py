def is_odd_even(number):
    """
    Check if a number is odd or even.

    Parameters:
    - number (int): The number to check.

    Returns:
    - str: 'Even' if the number is even, 'Odd' if the number is odd.
    """
    return 'Even' if number % 2 == 0 else 'Odd'