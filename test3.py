from jira import JIRA
import openai
import subprocess
from flask import Flask, request, jsonify
import requests
from cicd_functions import label_github_issue_with_jira


openai.api_type = "azure"
openai.api_base = "https://xorsdlcgenai.openai.azure.com/"
openai.api_version = "2023-07-01-preview"
openai.api_key = "c02b9c1085464e858fc26133aa8f190a"

jira_options = {'server': 'https://xoriant-sdlc.atlassian.net'}
github_token = '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'
github_repo = 'xor-singhakanksha/sdlc-test'  # Replace with your username and repository
headers = {
    'Authorization': f'token {github_token}',
    'Accept': 'application/vnd.github.v3+json'
}



gitlab_api_url = "https://gitlab.com/api/v4/projects/xor-singhakanksha%2Fsdlc-test/issues"
gitlab_token ="glpat-CwJy3LpbYA83a3zLzoge"
headers = {
        "PRIVATE-TOKEN": gitlab_token,
    }



app = Flask(__name__)

    


def create_github_issue(title, body):
    headers = {
        "PRIVATE-TOKEN": gitlab_token,
    }

    data = {
        "title": title,
        "description": body,
    }

    response = requests.post(gitlab_api_url, headers=headers, data=data)

    if response.status_code == 201:
      return response.json()["id"]
    else:
        print(f"Failed to create GitLab issue. Status code: {response.status_code}")
    return None

def create_jira_ticket(summary, description, project_key, issuetype_name):
    print( 'iiiiiiiiiiiiiiiiiiiiiii')
    jira_client = JIRA(options=jira_options, basic_auth=('singh_as@xoriant.com', 'ATATT3xFfGF0-loWzpBfwE5boluVSHplM0bPT9KUNFRLZRaUq5QNXntNQltC5UOFlI_3YHDjroZ-3xPVDKC1EIivG3idLTPEnAwqncmKfI25nWZrkdz8kSwIrzf6eWGeagrVzI2wPE8tmx5Cim0ynnsbsoMX-nw8iMzJHsumq8wypximpT9By8A=B32E8613'))

    issue_dict = {
        'project': {'key': project_key},
        'summary': summary,
        'description': summary,
        'issuetype': {'name': issuetype_name},
    }
    new_issue = jira_client.create_issue(fields=issue_dict)
    return new_issue

def tag_github_issue_with_jira(gitlab_api_url, gitlab_issue_id, jira_issue_id, description):
    headers = {
        "PRIVATE-TOKEN": gitlab_token,
    }

    data = {
        "description":  f'Related JIRA issue: [{jira_issue_id}](https://xoriant-sdlc.atlassian.net/browse/{jira_issue_id})'
       
    }

    update_url = f"{gitlab_api_url}/{gitlab_issue_id}"
    response = requests.put(update_url, headers=headers, data=data)
    if response.status_code == 200:
        print("GitLab issue updated successfully.")
    else:
        print(f"Failed to update GitLab issue. Status code: {response.status_code}")



def create_related_branch(jiraid, issue_number):
        issue_url = f"https://gitlab.com/api/v4/projects/xor-singhakanksha%2Fsdlc-test/issues/{issue_number}"
        issue_info = subprocess.check_output(["curl", issue_url]).decode("utf-8")

        # Create a new branch
        branch_name = f"{jiraid}-1"
        subprocess.run(["git", "checkout", "-b", branch_name])
        issue_info = subprocess.check_output(["curl", issue_url]).decode("utf-8")
        issue_title = 'test'

        # Create a new branch

        # Create and push the tag
        tag_name = f"{jiraid}-1"
        res= subprocess.run(["git", "tag", "-a", tag_name, "-m", f"Issue #{issue_number}: {issue_title}"])
        print(res, '------------------')
        subprocess.run(["git", "push", "origin", branch_name, "--tags"])

        print(f"Branch '{branch_name}' and tag '{tag_name}' created and pushed successfully.")


def create_related_branch(issue_key, branch_name):
    base_branch="main"
    try:
        # Create a new branch
        subprocess.run(["git", "checkout", "-b", branch_name])

        # Make changes and commit
        # (Replace this section with your actual changes)
        with open("example.txt", "w") as f:
            f.write("Hello, this is a new file.")
        subprocess.run(["git", "add", "."])
        subprocess.run(["git", "commit", "-m", f"Implement feature for {issue_key}"])

        # Push the branch to the remote repository
        subprocess.run(["git", "push", "origin", branch_name])

        # Create a pull request (GitHub API example)
        github_token = "YOUR_GITHUB_TOKEN"  # Replace with your GitHub personal access token
        github_repo = "your-repository"     # Replace with your GitHub repository

        headers = {
        "PRIVATE-TOKEN": gitlab_token,
    }

        data = {
            "title": f"Implement feature for {issue_key}",
            "head": branch_name,
            "base": base_branch,
        }

        response = requests.post(f"{gitlab_api_url}", headers=headers, json=data)

        print(response, '---------------------')

        if response.status_code == 201:
            print(f"Branch '{branch_name}' and MR created successfully.")
        else:
            print(f"Failed to create MR. Status code: {response.status_code}, Response: {response.text}")

    except Exception as e:
        print(f"Error: {e}")

    
def add_web_link_to_jira_issue(jira_issue_key, web_url):  
    # URL for the JIRA issue remote link API  
    api_url = f'https://Team-jtzo0k70.atlassian.net/rest/api/3/issue/GPTFOR-10/remotelink'  
      
    # JSON payload for the web link  
    data = {  
        "object": {  
            "url": web_url,  
            "title": "GitHub Issue"  
        }  
    }  
      
    # Make the request to add the web link to the JIRA issue  
    response = requests.post(api_url, json=data, headers=headers, auth=HTTPBasicAuth(jira_username, jira_api_token))  
      
    # Check if the request was successful  
    if response.status_code == 201:  
        print(f"Successfully added web link to JIRA issue {jira_issue_key}")  
    else:  
        print(f"Failed to add web link to JIRA issue {jira_issue_key}")  
        print(f"Response: {response.content}")  



def create_branch_and_commit(issue_key, branch_name, commit_summary):
    try:
        # Create a new branch
        branch_name_with_issue = f"{issue_key}-{branch_name}"
        #subprocess.run(["git", "checkout", "-b", branch_name_with_issue])

        # Make changes and commit
        # (Replace this section with your actual changes)
        with open("example.txt", "w") as f:
            f.write("Hello, this is a new file.")
        subprocess.run(["git", "add", "."])
        subprocess.run(["git", "commit", "-m", f"{issue_key} {commit_summary}"])
        subprocess.run(["git", "config", "--local", "http.https://gitlab.com/.extraheader", f"Authorization: Bearer {gitlab_token}"])
        gitlab_repo_url = "https://gitlab.com/xor-singhakanksha/sdlc-test.git"
        res= subprocess.run(["git", "push", gitlab_repo_url, branch_name_with_issue])
        print(issue_key, commit_summary,res, '==============')
        print(f"Branch '{branch_name_with_issue}' created and commit linked to Jira issue successfully.")
    except Exception as e:
        print(f"Error: {e}")

@app.route("/api_jira_ticket", methods=["POST"])
def api_jira_ticket():


    # issue_key = "ST-2"
    # branch_name = "feature-branch-1"
    # commit_summary = "Implement new feature"
    # create_branch_and_commit(issue_key, branch_name, commit_summary)
    # return


    # Imagine you have a list of user stories
    user_stories = [
    "As a shopper, I want to filter products by category so that I can quickly find items I'm interested in.",
    "As a returning customer, I want to log in to my account using my email and password so that I can access my order history and saved preferences.",
    "As a first-time visitor, I want to browse products without creating an account so that I can decide whether I'm interested in the offerings.",
    "As a busy user, I want to have a one-click checkout option so that I can quickly complete my purchase without entering my details every time.",
    "As an environmentally conscious buyer, I want to see eco-friendly products highlighted so that I can make sustainable choices.",
    "As a user with visual impairment, I want the website to be accessible with screen readers so that I can navigate and shop independently.",
    "As a discount lover, I want to be notified of sales and special offers so that I can take advantage of lower prices.",
    "As a frequent shopper, I want to earn loyalty points with my purchases so that I can get discounts on future orders.",
    "As a gift buyer, I want to send products to a different address than my billing address so that I can easily send gifts to friends and family.",
    "As an international customer, I want to see prices in my local currency so that I can better understand how much I will be spending."
]
    gitlab_api_url = "https://gitlab.com/api/v4/projects/xor-singhakanksha%2Fsdlc-test/issues"
    #print('-------->>>>>>>>>>>>>----------',user_stories)
    for story in user_stories:
        #description = generate_ticket_description(story)
        description ="testing jira ticket creation"
        #print('------------------',description)
        
        # Assuming your user story has a title or summary
        summary = story.split("\n")[0]
        jira_ticket = create_jira_ticket(
            summary=summary,
            description=description,
            project_key='ST',
            issuetype_name='Story'
        )
       # print(f"Created JIRA ticket: {jira_ticket.key}")
        github_issue_number = create_github_issue(summary, description)
        print(jira_ticket.key,'----------->>>>>>>>>>>', github_issue_number)
        tagged_git_flag = tag_github_issue_with_jira(gitlab_api_url, github_issue_number, jira_ticket, description) 
        # branch_name = "feature-branch"
        # response=create_related_branch('ST-2',branch_name)
        # print('----------------------',response)
        # # print("-------->>>-------",tagged_git_flag)
        # # tagged_jira=label_github_issue_with_jira(github_repo, github_issue_number, jira_ticket)
        # # # print("---------------",tagged_jira)
        return "True"

def generate_ticket_description(user_story):
    # Create a prompt for the AI model
    prompt_text = f"Please provide a detailed description suitable for a JIRA ticket based on the following user story:\n\nUser Story: {user_story}\n\nDescription:"

    # Call the OpenAI API to generate the response
    completion = openai.ChatCompletion.create(
    engine="xorgpt4",
    messages=[{"role": "system", "content": prompt_text}],
    temperature=0.7,
    max_tokens=2000,
    top_p=0.95,
    frequency_penalty=0,
    presence_penalty=0,
    stop=None
    )

    # Extract the generated description from the response
    generated_description = completion.choices[0].message['content']

    return generated_description


if __name__ == '__main__':
    app.run(debug=True)
