from jira import JIRA
import openai
from flask import Flask, request, jsonify
import requests
from cicd_functions import label_github_issue_with_jira, get_latest_pipeline_report, create_jira_ticket, create_gitlab_issue, get_jira_issue_title, branch_exists_on_gitlab, branch_exists_locally, create_branch_commit_and_push, tag_github_issue_with_jira, add_attachment_to_jira, remove_characters
import subprocess
from git import Repo
from requests.auth import HTTPBasicAuth
from dotenv import load_dotenv
import os
import logging
load_dotenv()

logging.basicConfig(level=logging.ERROR)

JIRA_ISSUE_TYPE =os.getenv('JIRA_ISSUE_TYPE')
BASE_PATH= os.getenv('PROJECT_BATH_PATH')

app = Flask(__name__)

@app.route("/api_jira_ticket", methods=["POST"])
def api_jira_ticket():
    # Imagine you have a list of user stories
    user_stories = [
     {
         "summary": "As a shopper, I want to filter products by category so that I can quickly find items I'm interested in.",
         "description":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. "
     },
      {
         "summary": "As a returning customer, I want to log in to my account using my email and password so that I can access my order history and saved preferences..",
         "description":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. "
     }
]
   
    for story in user_stories:
        jira_ticket = create_jira_ticket(
            summary=story["summary"],
            description=story["description"],
            issuetype_name=JIRA_ISSUE_TYPE
        )

        print(jira_ticket, '---------------')
        if jira_ticket is not None:
           gitlab_issue = create_gitlab_issue(story["summary"], story["description"])
           print(gitlab_issue)
           if gitlab_issue["status_code"] == 201:
             gitlab_issue_id = gitlab_issue["gitlab_issue_id"]
             tagged_git_flag = tag_github_issue_with_jira(gitlab_issue_id, jira_ticket)
             print('ppppppppppppp', gitlab_issue_id, jira_ticket)
             return {"jira_ticket_id": str(jira_ticket), "gitlab_issue_id": gitlab_issue_id}
        else:
           print("Failed to create Jira ticket.")
           return "False"
        

@app.route("/api_remote_branch_MR_create", methods=["POST"])
def api_remote_branch_MR_create():
    data = request.get_json()
    git_issue_id = data['git_issue_id']
    jira_issue_id = "ST-" +str(data['jira_issue_id'])
    jira_title = get_jira_issue_title(jira_issue_id)
    commit_summary = jira_title.split()
    commit_summary = ' '.join(commit_summary[:10])


    lowercase_text = jira_title.lower()
    characters_to_remove = [',', '.']
    underscored_text =  remove_characters(lowercase_text, characters_to_remove)
    final_branch_name = underscored_text[:50]

    branch_name_with_issue = f"{jira_issue_id}-{final_branch_name}"
    commit_summary =  f"{jira_issue_id}  {commit_summary}"
    exists_remote = branch_exists_on_gitlab(branch_name_with_issue)
    exists_locally = branch_exists_locally(branch_name_with_issue)
    print(exists_remote,exists_locally, branch_name_with_issue, final_branch_name )
    
    if not exists_locally and not exists_remote:
     print(f"The branch '{final_branch_name}' doesn't exist locally and doesn't exist remotely.")
     response= create_branch_commit_and_push(jira_issue_id,git_issue_id, final_branch_name, commit_summary)

     print('rrrrrrrrrrrrrrr',response)
     return response
    elif not exists_locally:
     print(f"The branch '{final_branch_name}' doesn't exist locally.")
     return {"message": f"The branch '{final_branch_name}' doesn't exist locally."}
    elif not exists_remote:
     return {"message": f"The branch '{final_branch_name}' doesn't exist remotely."}
    else:
     print(f"The branch '{final_branch_name}' exists both locally and remotely.")
     return {"message": f"The branch '{final_branch_name}' does exist both locally and  remotely."}
    

@app.route("/api_check_failed_pipeline", methods=["GET"])
def api_check_failed_pipeline():
    artifact_path = get_latest_pipeline_report()
    file_path_to_upload =f'{BASE_PATH}\\{artifact_path}'
    resp= add_attachment_to_jira('ST-28', file_path_to_upload)
    return {"artifact_log_file":artifact_path}

if __name__ == '__main__':
    app.run(debug=True)
