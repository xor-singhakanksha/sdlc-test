# test_odd_even_checker.py

import unittest
import coverage  # Import the coverage package
from odd_even_checker import is_odd_even

class TestOddEvenChecker(unittest.TestCase):
    
    def test_even_number(self):
        result = is_odd_even(4)
        self.assertEqual(result, 'Even', "4 should be Even")

    def test_odd_number(self):
        result = is_odd_even(7)
        self.assertEqual(result, 'Odd', "7 should be Odd")

    def test_zero_is_even(self):
        result = is_odd_even(0)
        self.assertEqual(result, 'Even', "0 should be Even")

    def test_negative_odd_number(self):
        result = is_odd_even(-3)
        self.assertEqual(result, 'Odd', "-3 should be Odd")

    def test_positive_odd_number(self):
        result = is_odd_even(5)
        self.assertEqual(result, 'Even', "5 should be Odd")

if __name__ == '__main__':
    # Use coverage to run the tests and generate a coverage report
    cov = coverage.Coverage(source=['odd_even_checker'])
    cov.start()

    # Run the tests
    unittest.main()

    # Stop coverage and generate the HTML report
    cov.stop()
    cov.html_report(directory='htmlcov')
