from jira import JIRA
import openai
from flask import Flask, request, jsonify
import requests
from cicd_functions import label_github_issue_with_jira
import subprocess
from git import Repo


openai.api_type = "azure"
openai.api_base = "https://xorsdlcgenai.openai.azure.com/"
openai.api_version = "2023-07-01-preview"
openai.api_key = "c02b9c1085464e858fc26133aa8f190a"

jira_options = {'server': 'https://xoriant-sdlc.atlassian.net'}
github_repo = 'singhakanksha2007/shop-master'  # Replace with your username and repository



gitlab_api_url = "https://gitlab.com/api/v4/projects/singhakanksha2007%2Fshop-master/issues"
gitlab_token ="glpat-16rqKuCpsZ9Us9X2byBz"
headers = {
        "PRIVATE-TOKEN": gitlab_token,
    }



app = Flask(__name__)


def create_github_issue(title, body):
    headers = {
        "PRIVATE-TOKEN": gitlab_token,
    }

    data = {
        "title": title,
        "description": body,
    }

    response = requests.post(gitlab_api_url, headers=headers, data=data)
    print(response, '-----------------',  response.json())
    if response.status_code == 201:
      return response.json()["iid"]
    else:
        print(f"Failed to create GitLab issue. Status code: {response.status_code}")
    return None

def create_jira_ticket(summary, description, project_key, issuetype_name):
    print( 'iiiiiiiiiiiiiiiiiiiiiii')
    jira_client = JIRA(options=jira_options, basic_auth=('singh_as@xoriant.com', 'ATATT3xFfGF0-loWzpBfwE5boluVSHplM0bPT9KUNFRLZRaUq5QNXntNQltC5UOFlI_3YHDjroZ-3xPVDKC1EIivG3idLTPEnAwqncmKfI25nWZrkdz8kSwIrzf6eWGeagrVzI2wPE8tmx5Cim0ynnsbsoMX-nw8iMzJHsumq8wypximpT9By8A=B32E8613'))

    issue_dict = {
        'project': {'key': project_key},
        'summary': summary,
        'description': summary,
        'issuetype': {'name': issuetype_name},
    }
    new_issue = jira_client.create_issue(fields=issue_dict)
    return new_issue

def tag_github_issue_with_jira(gitlab_api_url, gitlab_issue_id, jira_issue_id, description):
    headers = {
        "PRIVATE-TOKEN": gitlab_token,
    }

    data = {
        "description":  f'Related JIRA issue: [{jira_issue_id}](https://xoriant-sdlc.atlassian.net/browse/{jira_issue_id})'
       
    }

    update_url = f"{gitlab_api_url}/{gitlab_issue_id}"
    response = requests.put(update_url, headers=headers, data=data)
    if response.status_code == 200:
        print("GitLab issue updated successfully.")
    else:
        print(f"Failed to update GitLab issue. Status code: {response.status_code}")

def add_web_link_to_jira_issue(jira_issue_key, web_url):  
    # URL for the JIRA issue remote link API  
    api_url = f'https://Team-jtzo0k70.atlassian.net/rest/api/3/issue/GPTFOR-10/remotelink'  
      
    # JSON payload for the web link  
    data = {  
        "object": {  
            "url": web_url,  
            "title": "GitHub Issue"  
        }  
    }  
      
    # Make the request to add the web link to the JIRA issue  
    response = requests.post(api_url, json=data, headers=headers, auth=HTTPBasicAuth(jira_username, jira_api_token))  
      
    # Check if the request was successful  
    if response.status_code == 201:  
        print(f"Successfully added web link to JIRA issue {jira_issue_key}")  
    else:  
        print(f"Failed to add web link to JIRA issue {jira_issue_key}")  
        print(f"Response: {response.content}") 


def create_branch_commit_and_push(issue_key, branch_name, commit_summary, gitlab_token):
    try:
        # Create a new branch
        branch_name_with_issue = f"{issue_key}-{branch_name}"
        #subprocess.run(["git", "checkout", "-b", branch_name_with_issue])

        # Make changes and commit
        # (Replace this section with your actual changes)
        with open("example.txt", "w") as f:
            f.write("Hello, this is a new file....")
        subprocess.run(["git", "add", "."])
        subprocess.run(["git", "commit", "-m", f"{issue_key} {commit_summary}"])

        # Set the http.extraHeader configuration for Git credential helper
        # subprocess.run(["git", "config", "--local", "http.https://gitlab.com/.extraheader", f"Authorization: Bearer {gitlab_token}"])
        print('hiiiiiiiiiiiiii')
        # Push the branch to the specific GitLab repository with headers
        # gitlab_api_url = "https://gitlab.com/api/v4/projects/xor-singhakanksha%2Fsdlc-test/issues"
        # subprocess.run(["git", "push", gitlab_api_url, branch_name_with_issue])

        # token="glpat-E441xhvQuHmxwpdaGV8H"
        # gitlab_repo_url = f"https://oauth2:{gitlab_token}@gitlab.com/singhakanksha2007/shop-master"
        # #gitlab_repo_url = f"https://{gitlab_token}@gitlab.com/xor-singhakanksha/sdlc-test.git"
        # test=subprocess.run(["git", "push", gitlab_repo_url, branch_name_with_issue], capture_output=True)
        # print('hiiiiiiiiiiiiii22222222222', gitlab_repo_url, test)
        # print(test.stdout.decode())
        # print(test.stderr.decode())

        subprocess.run(["git", "push", "git@gitlab.com:singhakanksha2007/shop-master.git", branch_name_with_issue])
              

        # print(f"Branch '{branch_name_with_issue}' created, commit linked to Jira issue, and branch pushed to GitLab successfully.")
    except Exception as e:
        print(f"Error: {e}") 

@app.route("/api_jira_ticket", methods=["POST"])
def api_jira_ticket():

    # github_issue_number = create_github_issue('test', 'test description')
    # print('0000000000000000000>>>>>>>>', github_issue_number)
    issue_key = "ST-2"
    branch_name = "feature-branch-2"
    commit_summary = "Implement new feature"
    create_branch_commit_and_push(issue_key, branch_name, commit_summary, gitlab_token)
    return

    # Imagine you have a list of user stories
    user_stories = [
    "As a shopper, I want to filter products by category so that I can quickly find items I'm interested in.",
    "As a returning customer, I want to log in to my account using my email and password so that I can access my order history and saved preferences.",
    "As a first-time visitor, I want to browse products without creating an account so that I can decide whether I'm interested in the offerings.",
    "As a busy user, I want to have a one-click checkout option so that I can quickly complete my purchase without entering my details every time.",
    "As an environmentally conscious buyer, I want to see eco-friendly products highlighted so that I can make sustainable choices.",
    "As a user with visual impairment, I want the website to be accessible with screen readers so that I can navigate and shop independently.",
    "As a discount lover, I want to be notified of sales and special offers so that I can take advantage of lower prices.",
    "As a frequent shopper, I want to earn loyalty points with my purchases so that I can get discounts on future orders.",
    "As a gift buyer, I want to send products to a different address than my billing address so that I can easily send gifts to friends and family.",
    "As an international customer, I want to see prices in my local currency so that I can better understand how much I will be spending."
]
    gitlab_api_url = "https://gitlab.com/api/v4/projects/xor-singhakanksha%2Fsdlc-test/issues"
    #print('-------->>>>>>>>>>>>>----------',user_stories)
    for story in user_stories:
        #description = generate_ticket_description(story)
        description ="testing jira ticket creation"
        #print('------------------',description)
        
        # Assuming your user story has a title or summary
        summary = story.split("\n")[0]
        jira_ticket = create_jira_ticket(
            summary=summary,
            description=description,
            project_key='ST',
            issuetype_name='Story'
        )
        print(f"Created JIRA ticket: {jira_ticket.key}")
        github_issue_number = create_github_issue(summary, description)
        print(jira_ticket.key,'----------->>>>>>>>>>>', github_issue_number)
        tagged_git_flag = tag_github_issue_with_jira(gitlab_api_url, github_issue_number, jira_ticket.key, description) 
        # print("-------->>>-------",tagged_git_flag)
        # tagged_jira=label_github_issue_with_jira(github_repo, github_issue_number, jira_ticket)
        # # print("---------------",tagged_jira)
        return "True"

def generate_ticket_description(user_story):
    # Create a prompt for the AI model
    prompt_text = f"Please provide a detailed description suitable for a JIRA ticket based on the following user story:\n\nUser Story: {user_story}\n\nDescription:"

    # Call the OpenAI API to generate the response
    completion = openai.ChatCompletion.create(
    engine="xorgpt4",
    messages=[{"role": "system", "content": prompt_text}],
    temperature=0.7,
    max_tokens=2000,
    top_p=0.95,
    frequency_penalty=0,
    presence_penalty=0,
    stop=None
    )

    # Extract the generated description from the response
    generated_description = completion.choices[0].message['content']

    return generated_description


if __name__ == '__main__':
    app.run(debug=True)
